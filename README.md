# User.js tweaks
The main goal of this repository is to make available a bunch of hardening and performance tweaks.

## Assumptions
- You don't need search suggestions or search engines to be updated automatically.
- Pocket, Hello, VR, gamepads and other things are unnecessary bloat.
- You have no intention of playing media (i.e. YouTube is probably broken).
- You don't use the devtools via the right-click context menu (can still use F12).
- You're not fussed by about:newtab and such.
- You're happy to use pipelining (including over SSL).
- Addons do not need to be automatically updated.
